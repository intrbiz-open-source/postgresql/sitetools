#!/bin/sh

# configure nginx
echo "Setting HTTP Port to $HTTP_PORT"
sed -i "s/HTTP_PORT/$HTTP_PORT/" /etc/nginx/nginx.conf

# Exec nginx
echo "Starting nginx"
exec /usr/sbin/nginx -g "daemon off;" -c /etc/nginx/nginx.conf
