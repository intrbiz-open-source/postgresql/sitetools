##
## A simple container to run a PG EU Static Site
##
FROM registry.opensuse.org/opensuse/tumbleweed:latest AS balsa-base
MAINTAINER Chris Ellis <chris@intrbiz.com>
RUN zypper -q -n ref && zypper -q -n in --no-recommends python311 bash python311-scour python311-cssmin python311-Jinja2 python311-Markdown python311-dateutils git nginx sed && zypper -q -n clean

## Build the site
RUN mkdir /site
COPY deploystatic.py /site
COPY build.sh /site
COPY /site /site/site
COPY context.container.json /site/site/templates/context.container.json
WORKDIR /site
RUN ./build.sh

## Setup NGINX
RUN mkdir /etc/nginx/server.d
COPY nginx.conf /etc/nginx/nginx.conf
COPY entrypoint.sh /
RUN chmod 755 /entrypoint.sh
RUN chown -R nginx:nginx /site
RUN chown -R nginx:nginx /etc/nginx
USER nginx
EXPOSE 8080
ENV HTTP_PORT=8080
ENTRYPOINT [ "/entrypoint.sh" ]
CMD []
