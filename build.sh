#!/bin/bash
BASE=$(cd $(dirname $0); pwd)
REPO=site
GITHASH=$(cd $BASE/$REPO; git rev-parse --short HEAD)

# clean out dest
rm -rf $BASE/dest
mkdir -p $BASE/dest

# update source stuff
pushd $BASE/$REPO/static/css/src
./build-css.sh
popd
pushd $BASE/$REPO/static/img
./optimise_svgs.sh
popd

# main build
python3 $BASE/deploystatic.py $BASE/$REPO $BASE/dest
mv $BASE/dest/static $BASE/dest/$GITHASH
mkdir $BASE/dest/static
mv $BASE/dest/$GITHASH $BASE/dest/static/
